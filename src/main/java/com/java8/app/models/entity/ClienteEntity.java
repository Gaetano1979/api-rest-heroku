package com.java8.app.models.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "clientes")
public class ClienteEntity implements Serializable {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "nombre",nullable = false)
	private String nombre;

	@Column(name = "apellido",nullable = false)
	private String apellido;

	@Column(name = "email", unique = true,nullable = false)
	private String email;

	@Column(name = "create_at")
	@Temporal(TemporalType.DATE)
	private Date createAt;
	
	@PrePersist
	public void newDate() {
		this.createAt = new Date();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long idLong) {
		this.id = idLong;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombreString) {
		this.nombre = nombreString;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellString) {
		this.apellido = apellString;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String emailString) {
		this.email = emailString;
	}

	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}
	
	public void setCliente(String nombre,String apellido, String email) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.email = email;
	}

	private static final long serialVersionUID = 1L;
}
