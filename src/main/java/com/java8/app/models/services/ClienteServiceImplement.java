package com.java8.app.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.java8.app.models.dao.IClienteDao;
import com.java8.app.models.entity.ClienteEntity;

@Service
public class ClienteServiceImplement implements IClienteServices {
	
	@Autowired
	private IClienteDao clienteDao;

	@Override
	@Transactional(readOnly = true)
	public List<ClienteEntity> findAllClienteEntities() {
		// TODO Auto-generated method stub
		return (List<ClienteEntity>) clienteDao.findAll();
	}

	@Override
	public ClienteEntity findByIdClienteEntity(Long idCliente) {
		// TODO Auto-generated method stub
		return clienteDao.findById(idCliente).orElse(null);
	}

	@Override
	public ClienteEntity saveClienteEntity(ClienteEntity cliente) {
		// TODO Auto-generated method stub
		return clienteDao.save(cliente);
	}

	@Override
	public void deleteClienteEntity(Long idCliente) {
		// TODO Auto-generated method stub
		clienteDao.deleteById(idCliente);
	}

}
