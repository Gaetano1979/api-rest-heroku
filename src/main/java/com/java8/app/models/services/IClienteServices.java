package com.java8.app.models.services;

import java.util.List;

import com.java8.app.models.entity.ClienteEntity;

public interface IClienteServices {
	
	public List<ClienteEntity> findAllClienteEntities();
	
	public ClienteEntity findByIdClienteEntity(Long idCliente);
	
	public ClienteEntity saveClienteEntity(ClienteEntity cliente);
	
	public void deleteClienteEntity(Long idCliente);

}
