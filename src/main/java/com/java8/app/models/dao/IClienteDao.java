package com.java8.app.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.java8.app.models.entity.ClienteEntity;

public interface IClienteDao extends CrudRepository<ClienteEntity, Long> {

}
