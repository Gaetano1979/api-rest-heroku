package com.java8.app.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import com.java8.app.models.entity.ClienteEntity;
import com.java8.app.models.services.IClienteServices;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class ClientesControlesErrores {

	@Autowired
	private IClienteServices clienteServices;

	// Todos los clientes con manejo de errores
	@GetMapping("/clientesErrores")
	public ResponseEntity<?> listaclientes() { // ResponseEntity de forma generica

		List<ClienteEntity> clientesClienteEntities = null; // creamos una lista de los clientes
		Map<String, Object> responseMap = new HashMap<>(); // creamos un Map para manejar los errores

		try {
			clientesClienteEntities = clienteServices.findAllClienteEntities();
		} catch (DataAccessException e) {
			responseMap.put("messaje", "Error en cargar la lista"); // creamos el mensaje
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		// condiciones se la lista clientes esta vacia
		if (clientesClienteEntities.size() == 0) {
			responseMap.put("messaje", "No hay clientes en la lista");
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.OK); // retornamos el mensaje con
																						// estatus
		}

		responseMap.put("clientes", clientesClienteEntities);
		responseMap.put("total clientes", clientesClienteEntities.size());

		return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.OK);
		// return new ResponseEntity<List<ClienteEntity>>(clientesClienteEntities,
		// HttpStatus.OK);
	}

	// Creamos el Post Cliente
	@PostMapping("/clientesErrore")
	public ResponseEntity<?> crearCliente(@RequestBody ClienteEntity cliente) {

		ClienteEntity clienteEntity = null;
		Map<String, Object> responseMap = new HashMap<>();

		try {
			clienteEntity = clienteServices.saveClienteEntity(cliente);
		} catch (DataAccessException e) {

			responseMap.put("messaje", "Error en ingresar el cliente");
			responseMap.put("error", e.getMostSpecificCause().getMessage());
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		responseMap.put("cliente", clienteEntity);
		responseMap.put("message", "Cliente insertado corectamente");

		return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.CREATED);

	}

	// Creamos el Put Cliente
	@PutMapping("/clientesErrore/{id}")
	public ResponseEntity<?> updatecliente(@RequestBody ClienteEntity cliente, @PathVariable(name = "id") Long id) {

		Map<String, Object> responseMap = new HashMap<>();

		String nombre = cliente.getNombre();
		String apellido = cliente.getApellido();
		String email = cliente.getEmail();

		ClienteEntity clienteActualClienteEntity = clienteServices.findByIdClienteEntity(id);
		ClienteEntity clienteUpClienteEntity = null;

		if (clienteActualClienteEntity == null) {
			responseMap.put("messaje", "No se pudo encontar el cliente en la BD");
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NOT_FOUND);
		}

		try {
			clienteActualClienteEntity.setCliente(nombre, apellido, email);
			clienteUpClienteEntity = clienteServices.saveClienteEntity(clienteActualClienteEntity);
		} catch (DataAccessException e) {
			responseMap.put("messaje", "Error en actualizar el Cliente");
			responseMap.put("error", e.getMostSpecificCause().getMessage());
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		responseMap.put("cliente", clienteUpClienteEntity);
		responseMap.put("message", "El cliente ".concat(nombre).concat(" se ha actualizado corectamente"));
		return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.CREATED);
	}

	// Creamos el metodo Delete
	@DeleteMapping("/clientesErrore/{id}")
	public ResponseEntity<?> deleteCliente(@PathVariable(name = "id") Long id) {
		
		Map<String, Object> responseMap = new HashMap<>();
		ClienteEntity clienteActuale = clienteServices.findByIdClienteEntity(id);
		
		if (clienteActuale == null) {
			responseMap.put("error", "cliente con id: ".concat(id.toString()).concat(" no existe en la BD"));
			return new ResponseEntity<Map<String, Object>>(responseMap,HttpStatus.NOT_FOUND);
		}
		
		try {
			clienteServices.deleteClienteEntity(id);
		} catch (DataAccessException e) {
			responseMap.put("error", e.getMostSpecificCause().getMessage());
			responseMap.put("messaje", "Error en borrar el cliente".concat(clienteActuale.getNombre()));
			return new ResponseEntity<Map<String,Object>>(responseMap,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		responseMap.put("messaje", "Cliente ".concat(clienteActuale.getNombre()).concat(" se ha eliminado corectmante"));
		responseMap.put("cliente", clienteActuale);
		return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.FOUND);
	}

}
