package com.java8.app.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.java8.app.models.entity.ClienteEntity;
import com.java8.app.models.services.IClienteServices;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class ClienteController {

	@Autowired
	private IClienteServices clienteServices;

	@GetMapping("/clientes")
	public List<ClienteEntity> clientes() {
		return clienteServices.findAllClienteEntities();
	}

	// resposta sin errores
	/*
	 * @GetMapping("/clientes/{id}") public ClienteEntity show(@PathVariable(name =
	 * "id")Long id) { return clienteServices.findByIdClienteEntity(id); }
	 */

	// resposta con errores
	@GetMapping("/clientes/{id}")
	public ResponseEntity<?> show(@PathVariable(name = "id") Long id) {

		ClienteEntity clienteEntity = null;
		Map<String, Object> responseMap = new HashMap<>();

		try {
			clienteEntity = clienteServices.findByIdClienteEntity(id);
		} catch (DataAccessException e) {
			// TODO: handle exception
			responseMap.put("messaje", "Error en realizar la consulta");
			responseMap.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		if (clienteEntity == null) {
			responseMap.put("messaje",
					"El cliente con id: ".concat(id.toString().concat(" no existe en la base de datos")));
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<ClienteEntity>(clienteEntity, HttpStatus.OK);
	}

	@PostMapping("/clientes")
	@ResponseStatus(code = HttpStatus.CREATED)
	public ClienteEntity crearCliente(@RequestBody ClienteEntity cliente) {

		return clienteServices.saveClienteEntity(cliente);
	}

	@PutMapping("/clientes/{id}")
	@ResponseStatus(code = HttpStatus.CREATED)
	public ClienteEntity postClienteEntity(@RequestBody ClienteEntity cliente, @PathVariable(name = "id") Long id) {

		ClienteEntity clienteactualizadoClienteEntity = clienteServices.findByIdClienteEntity(id);
		clienteactualizadoClienteEntity.setNombre(cliente.getNombre());
		clienteactualizadoClienteEntity.setApellido(cliente.getApellido());
		clienteactualizadoClienteEntity.setEmail(cliente.getEmail().toLowerCase());
		return clienteServices.saveClienteEntity(clienteactualizadoClienteEntity);

	}

	@DeleteMapping("/clientes/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void deleteCliente(@PathVariable(name = "id") Long id) {
		clienteServices.deleteClienteEntity(id);
	}

}
