package com.java8.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiRestJava8Application {

	public static void main(String[] args) {
		SpringApplication.run(ApiRestJava8Application.class, args);
	}

}
